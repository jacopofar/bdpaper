
%%%%%%%%%%%%%%%%%%%%%%% file typeinst.tex %%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is the LaTeX source for the instructions to authors using
% the LaTeX document class 'llncs.cls' for contributions to
% the Lecture Notes in Computer Sciences series.
% http://www.springer.com/lncs       Springer Heidelberg 2006/05/04
%
% It may be used as a template for your own input - copy it
% to a new file with a new name and use it as the basis
% for your article.
%
% NB: the document class 'llncs' has its own and detailed documentation, see
% ftp://ftp.springer.de/data/pubftp/pub/tex/latex/llncs/latex2e/llncsdoc.pdf
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\documentclass[runningheads,a4paper]{llncs}

\usepackage{amssymb}
\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{url}
\usepackage{color}
\newcommand{\hilight}[1]{\colorbox{yellow}{#1}}
\urldef{\mailsa}\path|{jacopo1.farina,|
\urldef{\mailsb}\path|seconda.mail, terza mail}@mail.polimi.it|
\newcommand{\keywords}[1]{\par\addvspace\baselineskip
\noindent\keywordname\enspace\ignorespaces#1}

\begin{document}

\mainmatter  % start of an individual contribution

% first the title is needed
\title{Massive public messages analysis with Big Data:
\\extraction, sentiment analysis and representation
\\with linearly scalable techniques}

% a short form should be given in case it is too long for the running head
\titlerunning{Public message analysis with Big Data}

% the name(s) of the author(s) follow(s) next
%
% NB: Chinese authors should write their first names(s) in front of
% their surnames. This ensures that the names appear correctly in
% the running heads and the author index.
%

% si può scrivere \thanks{messaggio} per un footnote

\author{Jacopo Farina
\and nome2 \and nome3}
%
\authorrunning{Public message analysis with Big Data}
% (feature abused for this document to repeat the title also on left hand pages)

% the affiliations are given next; don't give your e-mail address
% unless you accept that it will be published
\institute{Politecnico di Milano,\\
Piazza Leonardo da Vinci 32, 20133 Milano, Italy\\
\mailsa\\
\mailsb\\
\url{http://www.polimi.it}}

%
% NB: a more complex sample for affiliations and the mapping to the
% corresponding authors can be found in the file "llncs.dem"
% (search for the string "\mainmatter" where a contribution starts).
% "llncs.dem" accompanies the document class "llncs.cls".
%

\toctitle{Public message analysis with Big Data}
\tocauthor{Jacopo Farina}
\maketitle


\begin{abstract}
This work describes the design and implementation of tools to extract,
 analyze and explore an arbitrarily great amount of public messages
 from diverse sources, exploiting a variety of new technologies for
 parallel and scalable processing like MapReduce. These tools aim to be quickly adaptable
 to different use cases, languages, and message sources.

First, to address the problem of the extraction of messages from large sized
 web forums not providing API access and with content possibly loaded through AJAX,
 an highly parallel scraper has been implemented, allowing the user to customize
 the behavior with scripting technologies and thus being able to manage dynamically loaded content.

Then, a novel framework is developed to support agile programming and
 thus easing the building and validation of a classifier for sentiment analysis.

Finally, a web application allows the real-time selection and projection
 of the analysis results in different dimensions in an OLAP fashion.
\keywords{sentiment analysis, map reduce, ETL,  OLAP analysis}
\end{abstract}

\section{Introduction}

The increasing usage of networks and the improvement of 
communication technologies has made it possible to access a previously
 unimaginable amount of messages written by the general public. For a
 company can be useful to exploit these messages to get an insight
 into the amount of satisfaction or dissatisfaction expressions,
 called \emph{sentiment}, occurring along the names of products and brands.
 
Sentiment occurs in various forms and grades, therefore it is necessary to
 identify a set of possible outcomes for a classifier among the possible
 ones which fits the use case. The discipline which studies the design and implementation
 of tools able to automatically detect the sentiment of a text is called \emph{sentiment analysis}.

It is interesting to detect how the sentiment changes over time, in different
 geographical zones and within different keywords to focus on specific
 aspects of the products.

Moreover, it is useful to get an insight about the differences in sentiment
 from various sources, for instance the comments of YouTube videos or FaceBook pages, hence
 allowing a company to find the ones having the highest impact in terms
 of number of messages and expressed sentiment.

Our goal is to analyze comments written in a wide time span, in some cases more than 10 years in a reasonable time,
not a stream of comments in real time as is frequent for this kind of task\cite{realtimesent}.

Since the amount of available messages can be huge and is rapidly growing
 year after year, it is necessary to resort to the use of techniques exploiting
 networks of computers able to process in parallel several distinct portions
 of the overall input data.

Although recent technologies like \emph{MapReduce}\cite{mapreduce} facilitate
 the development of such distributed applications managing the most common
 issues emerging from parallel environments, the implementation of these application is still
 more difficult than non-distributed ones, because of the greater
 effort and time consumption required for detecting problems and errors,
 finding the corresponding corrections and apply them to the different nodes.

The development of a sentiment analysis classifier, conversely, requires
 some sort of agile development to quickly try variations and improvements of
 the algorithm and validate them, hence the need of a framework enabling developers to run the same application
 in a local environment, quickly testing and validating it with a small input
 data sample, and, without modifications, in a distributed environment capable
 of scale linearly managing great amounts of user messages.

Furthermore, the real-time selection and graphical representation of millions
 of messages in the various dimensions (time, place, keywords, sources and sentiment)
 requires an accurate selection and adaptation of specific technologies.
 
\section{Use case}
One or more web forums contain millions of messages on a specific topic, written over time by a consistent amount of users from different places. A company distributing a product or service related to that topic is interested in getting an overall view of the opinions among users, extracting relevant trends and relations between product names, places, keywords and, above all, sentiments.
Not knowing beforehand which relations and trends express useful knowledge, our approach is to classify and index data along these dimensions and let the user freely explore and select through an OLAP-like analysis.
From a web forum containing, among others, the message
\begin{lstlisting}[breaklines]
Seen the last model yesterday at Chicago, not so exciting :(
\end{lstlisting}
we need to traverse pages, extract the message text from the page HTML within the publication date, detect the citation of the place \textit{Chicago}, the negative sentiment, the title of the topic and further allow the user to get a visual representation of how many negative posts were written in Illinois over the last 3 years, grouped by month, with an immediate query response even in case of millions of data points.
\section{State of the art and adopted technologies}
While most of the social networks offer API to developers in order to allow,
 the extraction of messages, web forums usually consist in dynamic web pages
 designed for browsers. Thus, to extract data from these pages is necessary
 to use a \emph{scraper}, a tool which download pages and follow HTML links inside them.
 
This task can be very time consuming since a site can consist in millions of pages, so
 scrapers use parallelization to fetch many pages simultaneously and filters to avoid unwanted pages.

In some cases, web pages contains scripts, run by the browser, which trigger
 the loading of the actual content; in this cases, a scraper will not extract
 it since it does not run page scripts. Running all of the page scripts requires
 to implement or adapt a very complex software and makes scraping several
 degrees of magnitude slower. The proposed solution is to allow the user
 to quickly define the behavior of the scraper in order to trigger the loading
 of the same documents called by page scripts.

Currently, the most sophisticated tool is \emph{Apache Nutch}, a distributed scraper
 aiming to create an index of one or more websites visiting pages and following URLs,
 which can scale linearly increasing the number of clusters and building an index.
 
However, it is aimed to index pages for further searches and not to extract
 specific data, thus does not suit the use case of post extraction from forums.
 Moreover, there is a limit on the speed of page extraction from single websites
 making pointless such an heavy solution, while content loaded with AJAX is not
 considered.

As for distributed processing, MapReduce is a paradigm allowing the development of a distributed application
 through the definition of two functions, \emph{map} and \emph{reduce},
 allowing the programmer to distribute the application on multiple nodes with
 the minimum work, not worrying about which node will process which chunk of input data
 and reassigning work and data in case of node failure.

\emph{Apache Hadoop} is the state-of-the-art implementation of
 the MapReduce paradigm, it implements a distributed filesystem, \textit{HDFS}, 
 which automatically divide files in blocks and store different copies of each block in different nodes.

Using Hadoop, a MapReduce \textit{job}, consisting in a map or reduce function execution,
 is divided in \textit{tasks}, which are assigned to the nodes by a central coordinator called \textit{JobTracker}. Hadoop aims to reduce the network traffic assigning tasks to the nodes owning the chunks to process; in case of failures or excessive delays, however, the same task is assigned to distinct nodes and chunks are replicated to avoid data loss.

A map function takes as an input a key-value pair $<k_{1},v_{1}>$, and emits a list of key-value pairs $<k_{2},v_{2}>$, which may be empty.

A reduce function receives as an input a key $k_{2}$ and the list of associated values $\{v_{2},v_{3},v_{4}...\}$ emitted by the map function, generating another set of pairs $<k_{3},v_{5}>$, which is the result of the process and could be further used by other MapReduce operations.

Sometimes one of the two function is the identity function, that is, can be omitted. For example, a map function could be used as a filter to emit only pairs corresponding to some criteria, thus the reduce function will be omitted and the result of the job will be the filtered input data.

While greatly leveraging the application development by managing common
 distributed applications problems, the deploying and execution of them
 on Hadoop clusters is very slow when the data is relatively small, due
 to heavy initialization procedures and data redundant replication.

\emph{Sentiment Analysis} is the possibility to automatically classify the mood
 expressed by a document and, in some cases, the subject of emotionally expressive utterances\cite{sentsubject}.

Currently, there are not widely used toolkit nor datasets for sentiment analysis,
 due to variety of possible definitions of the task. On the other hand,
 scientific literature regarding the models suitable for sentiment analysis is rich\cite{primoscholar}\cite{sentnlp}.
 
Languages different than English, however, are less often subject of work and coped using automatic translation \cite{senttranslate}, which gives poor results when applied to internet language\cite{translateslang} 

Various approaches exploit databases of words expressing sentiments\cite{sentiwordnet}; in this case this is not feasible, since handmade datasets are limited to one or a few languages and suffer of a low accuracy


%\section{Structure of the proposed solution}
\section{System Architecture}
We now illustrate the details of the framework we propose. Our system consists of the chain shown in Figure~\ref{fig:scheme}, where 
each oval represents data and each box describes operations performed on the data.

\begin{figure}
\centering
\includegraphics[height=1.8cm]{scheme_ETL}
\caption{The application scheme, squared blocks represent the  operations, ovals represent data.}
\label{fig:scheme}
\end{figure}
 
 Intuitively, our system takes as input a defined set of data sources (such as data coming from social networks, forums and so on) and
 produces, through the \textit{extractor} component, raw unstructured text messages. These messages are the input of the \textit{analysis} 
 component that generates knowledge by enriching each message with: (i) sentiment and (ii) geographical provenience.
 
This analysis is supported by a statistical model of sentiment, which uses a set of messages already assigned by hand to sentiment classes in order to automatically classify new messages, an operation which will be exposed later.
 
The described knowledge is then stored along with the original text as indexed structured data and is used as input for the \textit{reporting
and exploration} component of the chain. This last component allows the final user to run queries over the data; in particular it supports 
common OLAP operations such as drill down and slice and dice.

Thus, our system can be considered as a middle ground between an ETL and an OLAP tool. In fact, the first two components in our
framework perform  ETL operations, that is, extract data from web sources, transform it in knowledge and finally load it into an OLAP 
component that implements OLAP operations over the data.

All three components in our framework are designed to be used by users with little or no knowledge of sentiment analysis and distributed 
processing technologies and methods. 
% requiring only some familiarity with JavaScript and HTML.
The OLAP interface, in particular, is entirely graphical, allowing non expert users 
%a person not skilled in programming
to explore data autonomously.
 
 
In the following, we explain the details of the three proposed components.

\subsection{Extraction}
The extractor component consists in a scraper that has been implemented intensively
 exploiting multithreading and customization through Mozilla Rhino\footnote{\url{https://developer.mozilla.org/en-US/docs/Rhino}}
 scripting. The component allows users to quickly redefine the behavior of the tool, for example, by setting to avoid or redirect the URLs that have to be examined, 
 or to select and extract only small sections of the pages or yet, to download dynamic content. This leads to a rapid but strong customization
 of the tool behavior.
 
High levels of customization are usually not allowed by existing tools (such as \emph{httrack} and \emph{Apache Nutch}) that generally allow only  to specify URLs that have to be avoided or followed
using regular expressions. Moreover, dynamically loaded content, like AJAX\footnote{\url{https://en.wikipedia.org/w/index.php?title=Ajax_(programming)&oldid=553459243}},
 cannot be easily extracted. 
 
Our scraper has a default behavior which consists in a graph search leading to download all the pages and
 following all the links until all URLs found in the traversal are examined.

When the scraper downloads a page, the XHTML code is parsed and the compiled
 script is called, leaving to the user the task to define which elements
 are to be extracted, if any, and which URLs are to be followed whether
 the user need to avoid the default behavior of following all the hyperlinks.
The user script can enqueue arbitrary addresses to simulate AJAX calls, hence
 allowing to manage dynamically loaded content, and JSON annotations can
 be added to each pending URL allowing the user script to maintain a context during page traversal.

A web forum can thus be parsed efficiently defining a script usually
 shorter than 20 lines and extracting messages in this form:
 \begin{lstlisting}[breaklines]
{"post":"Seen the last model yesterday at Chicago, not so exciting :(",
    "date":"2013-02-16 10:34:00"
}
 \end{lstlisting}
The JSON string contains the raw text and the date of the post, and will be used by next steps.

A Bloom filter\cite{bloom} is a probabilistic set which offers two operation, \textit{add} an element and \textit{check} whether an element is already inside.
Both operations are done in $O(1)$ time and the data structure uses a fixed amount of memory, but the \textit{check} operation can lead to false positive, that is, could suggest the presence of an element which is not actually in the set. The probability of false positives is predictable and depends on the amount of memory used. 
The tool uses a Bloom filter to keep the examined URLs list in
 volatile memory thus scaling up to millions of pages examined on commodity
 hardware without accessing the disk for lookup, at the cost of a small (e.g. $<10^{-8}$ ) and
 predictable ratio of valid URLs ignored.

Thanks to the filter, it can be deployed, monitored and modified on a laptop
 and updated in real time fastening the procedure, which can last various days
 for internet forums.
 
\subsection{Analysis}
 As previously explained, the analysis component takes as input the raw data collected by the extractor and enriches it with some knowledge. First, text is put in lowercase and some patterns (money amounts, percentages, numbers, emoticons, etc.) are replaced with placeholders, obtaining for instance
 \begin{lstlisting}[breaklines]
seen the last model yesterday at chicago, not so exciting SMILENEG
 \end{lstlisting}
where \textit{"SMILENEG"} is a placeholder for the negative smile \textit{:(}
Text can be subjected to stemming to increase accuracy \cite{sent-stemming}, thus an heuristic stemmer \cite{snowball} was implemented using the framework and trained using a large amount of messages from a web forum. For instance, the previous statement can possibly become
 \begin{lstlisting}[breaklines]
se the last model yesterda at chicago, not so excit SMILENEG
 \end{lstlisting}
where the common suffix has been removed. The fact that the word "\textit{yestarday}"
 lost the ending \textit{y} is a mistake from a grammar point of view but
 correct with respect to the goal of removing conjugations, since no other word becomes "\textit{yesterda}" after stemming. 

Since a na\"{i}ve model gives poor results with aggregate terms, tokens within a distance $k$ are merged to form a richer feature vector and take into account whole expressions like "\textit{I don't like}" and not only single tokens.
The statement is thus divided in tokens using spaces and punctuation as token separator, then aggregate tokens are added, obtaining
\begin{lstlisting}[breaklines]
[se, yesterda, ... , SMILENEG, se_1_yesterda, ... ,excit_1_SMILEPOS,se_2_at, ... ,so_2_SMILENEG]
 \end{lstlisting}
where "\textit{so\_2\_SMILENEG}" represent the aggregate term made by "\textit{so}" followed by \textit{SMILENEG} after another token. Through this operation structures like "\textit{not\_2\_exciting}" can be maintained and considered in further steps.

Split text in single words is done using spaces and punctuation, and requires a tokenizer for languages like Chinese \cite{tokenchinese}.

Now the original message has become a set of tokens and thus can be threated as a boolean vector indicating the presence or absence of tokens. Using a set of statements labeled by hand with a sentiment, like 

 \begin{lstlisting}[breaklines]
{"post":":(", "sentiment":"negative"}
{"post":"Seen the last model yesterday at Chicago, not so exciting :(", "sentiment":"negative"}
{"post":"This book is short, but very exciting", "sentiment":"positive"}
 \end{lstlisting}

a list of tuples $\langle token, sentiment\rangle$ is generated, and the occurrences of each token in various classes are counted
 \begin{lstlisting}[breaklines]
{"token":"SMILENEG","negative":2}
{"token":"book","positive":1}
...
{"token":"excit","negative":1,"positive":1}
...
{"token":"not_2_excit","negative":1}
{"token":"ver_1_excit","positive":1}
...
 \end{lstlisting}
In this case, although the token \textit{excit(ing)} occurs in both sentiment classes, only "\textit{not\_2\_excit}" is associated with a negative sentiment.

The next step, not shown, is Laplacian smoothing, used to contrast overfitting\cite{smoothlaplace}, consisting in increasing by $1$ all the counters for all the tokens and classes. After smoothing, the polarity of each token for each class is calculated as the ratio of occurrences for that class on total occurrences. For example
 \begin{lstlisting}[breaklines]
...
{"token":"excit","negative":0.40,"positive":0.40,"neutral":0.20}
...
{"token":"not_2_excit","negative":0.50,"positive":0.25,"neutral":0.25}
{"token":"ver_1_excit","negative":0.25,"positive":0.50,"neutral":0.25}
...
 \end{lstlisting}

Using a set of messages assigned to sentiment classes by hand has been thus generated, for each class $i$, a vector$M_{i}$ assigning a polarity to each token.

The procedure to transform a previously unseen text in a vector is identical, but instead of token polarities only the presence is taken into account, generating a vector $S$ containing only $0$ or $1$. A token occurring multiple times in the same text is counted only once.

The dot product $S \cdot M_{i}$ gives a likelihood score to a pair of a text vector and sentiment class vector. The sentiment class giving the highest score for a text is chosen as the sentiment class of it. This likelihood is not normalized, that is, the sum of likelihoods for all the classes is not $1$, because a common normalizing positive factor has been ignored being irrelevant to the comparison of classes.

Once the polarities of tokens have been calculated from the manually classified messages, it is possible to apply them to classify new text by multiplying polarities of found tokens and assigning to the sentiment with the greatest result, obtaining a na\"{i}ve Bayes classifier.


The classifier has been tested on various datasets of messages from Twitter, Facebook, three web forums and YouTube, in Italian and English, or various mixed languages in the case of YouTube comments, trained and validated using the framework through k-folding with different values of $k$ and with or without the stemming, obtaining a precision, measured as the ratio of results matching with an human classifier between $0.42$ and $0.62$. It has to be noted that human evaluators has a disagreement rate for this task which can reach $0.4$ on short texts\cite{humandisagree}.
 
\subsection{Reporting and exploration}
Datasets are indexed using a relational database, which can scale linearly on a number of nodes under some conditions\cite{ospreydb}, namely PostgreSQL.

A set of analyzed and indexed messages, called \textit{datacube}, is an immutable data structure, slicing operations produce brand new datacubes, allowing many users to slice them without interfering with each other.

A web application allows to filter a datacube using time ranges, keywords, sentiments, place names, rectangles of geographical coordinates and any combination of these, generating a new datacube which can be further analyzed and filtered. A datacube can be represented along all the dimensions using specific representations (geographical heatmaps, sentiment histograms and pie charts, list of messages colored in accordance with sentiment, YouTube videos, FaceBook messages and forum threads with the most positive or negative sentiment of comments), intensively using HTML5 and AJAX to display data in an engaging and interactive application (see \ref{fig:cpanel_mappa_corse}) able to group data in real time, for instance an user can see sentiment trend histograms grouped by months, and years (e.g. all the posts made in january months over years) and at different time resolutions ranging from hours to years, just clicking links without reloading the page.

\begin{figure}
\centering
\includegraphics[width=6cm]{cpanel_mappa_corse}
\includegraphics[width=6cm]{cpanel_sentiment_mensile_assoluto}
\caption{An heatmap, showing the density of messages across different places, and an histogram showing the trends of sentiments across time. Both view are interactive, allowing the user to zoom, pan and change intervals, calculating results in real time.}
\label{fig:cpanel_mappa_corse}
\end{figure}

For each slicing operation, the application generates and display both the SQL query and the workflow code to run the same operation using Hadoop and the described framework, and allow the user to execute it directly.

Another advantage of such a web application is the possibility to visit immediately the sources of messages, like viewing the videos of YouTube from where the comments where extracted. 

To avoid conflicts between multiple simultaneous users datacubes are immutable:
 the result of a slicing operation is a new datacube, which can in turn
 be subject of other slicing operations, leaving untouched the original
 dataset and allowing the rapid visualization of different types of
 charts on the same selection.


 
\section{Framework for agile development}
The same paradigm of MapReduce, involving an abstract description of the
 operations on data, leaving to an external application the task of distributing
 jobs and data among the nodes, has been extended to abstract to the existence
 of a cluster at all, allowing a programmer to define a JavaScript workflow which will
 call a few generic operations or define its own through scripting.
 
The workflow calls various \textit{phases}, consisting of a map or reduce task (or both),
 specifying the input and output files and optionally some execution parameters in the form of a JSON object.
 Each phase defines map or reduce function as operations over JSON keys and values emitting in turn JSON keys and values, to allow the management of unstructured data, since these objects are seen as textual keys and values by Hadoop but can encapsulate dictionaries of arbitrary keys and values and nested objects;
 moreover, this format is human readable and can be efficiently compressed,
 easing the inspection of partial results without wasting disk space.

This workflow can hence be run seamlessly on a single node with a local file system or
 in a distributed environment using Hadoop, enabling agile development.

Through an intense use of scripting and JSON format, it is possible to rapidly
 define and test in local mode a workflow to manipulate unstructured data and deploy it
 to the cluster nodes without changes.
 
In case of Hadoop execution, the framework will embed phases in actual Hadoop
 tasks, converting JSON strings in Hadoop textual key-value pairs;
 in local mode the framework will read files, extract JSON objects and pass
 them to the phases, writing emitted output in the local file system and
 to the console allowing the user to monitor the application in real time.

\subsection{Sentiment analysis}
Sentiment analysis can be done with only two classes\cite{yesnopang}\cite{grief}, \textit{positive} and \textit{negative}, or multiple grades\cite{pangstar} and different dimensions (joy, frustration, anger, etc.), for this use case we adopted three classes: \textit{positive}, \textit{negative} and \textit{neutral}.

Various models and classifiers can be used, in general SVMs and Bayesian classifiers give the best results\cite{sentcompare}, the training of the latter type can be described as a MapReduce operation, hence it was chosen for this use case.

A statement can express various sentiments, directed toward distinct subjects or distinct features of the same subject (e.g. "\textit{Milan is beautiful but the weather is terrible}"). It is very difficult to automatically identify these elements inside statements, so most of the approaches to sentiment analysis try to detect the sentiment expressed in the whole text\cite{sentfrasi} assuming that texts in general have one main subject\cite{onesubject}, and under this assumption the application detect the mood of a text as a whole, ignoring distinct cited subjects.

\begin{thebibliography}{4}

\bibitem{mapreduce} Rob Pike, Sean Dorward, Robert Griesemer, Sean Quinlan: Interpreting the Data:
Parallel Analysis with Sawzall.
Special Issue on Grids and Worldwide Computing Programming Models and Infrastructure 13:4, pp. 227-298

\bibitem{bloom} Bloom, Burton H.,
Space/time trade-offs in hash coding with allowable errors
Communications of the ACM (1970)

\bibitem{ospreydb} YANG, Christopher, et al.
Osprey: Implementing MapReduce-style fault tolerance in a shared-nothing distributed database.
Data Engineering (ICDE), 2010 IEEE 26th International Conference on. IEEE, 2010. p. 657-668.

\bibitem{senttranslate}
Bautin, Mikhail; Vijayarenu, Lohit; Skiena, Steven.
International sentiment analysis for news and blogs
Proceedings of the International Conference on Weblogs and Social Media (ICWSM). 2008.

\bibitem{primoscholar}
Pang, Bo; Lee, Lillian.
Opinion mining and sentiment analysis
Foundations and trends in information retrieval, 2008

\bibitem{sentnlp}
Nasukawa, Tetsuya; Yi, Jeonghee.
Sentiment analysis: Capturing favorability using natural language processing.
Proceedings of the 2nd international conference on Knowledge capture. ACM, 2003. p. 70-77.

\bibitem{translateslang}
Clark, Eleanor; Araki, Kenji
Text normalization in social media: progress, problems and applications for a pre-processing system of casual English Procedia-Social and Behavioral Sciences, 2011, 27: 2-11

\bibitem{yesnopang}
Bo Pang and Lillian Lee and Shivakumar Vaithyanathan
Thumbs Up? Sentiment Classification Using Machine Learning Techniques
Proceedings of the ACL-02 conference on Empirical methods in natural language processing-Volume 10 (2002)

\bibitem{grief}
Benjamin Snyder; Regina Barzilay
Multiple Aspect Ranking using the Good Grief Algorithm
Proceedings of the Joint Human Language Technology/North American Chapter of the ACL Conference (2007)

\bibitem{pangstar}
Bo Pang and Lillian Lee
Seeing Stars: Exploiting Class Relationships For Sentiment Categorization With Respect To Rating Scales
Proceedings of ACL (2005), pp. 115--124.

\bibitem{sentiwordnet}
Esuli, Andrea; Sebastiani, Fabrizio.
Sentiwordnet: A publicly available lexical resource for opinion mining.
Proceedings of LREC. 2006. p. 417-422.

\bibitem{onesubject}
Meena, Arun; Prabhakar, T. V.
Sentence level sentiment analysis in the presence of conjuncts using linguistic analysis.
Advances in Information Retrieval. Springer Berlin Heidelberg, 2007. p. 573-580.

\bibitem{sentsubject}
Nasukawa, Tetsuya; Yi, Jeonghee.
Sentiment analysis: Capturing favorability using natural language processing.
Proceedings of the 2nd international conference on Knowledge capture. ACM, 2003. p. 70-77

\bibitem{sentcompare}
Mike Thelwall, Kevan Buckley, Georgios Paltoglou, Di Cai
Sentiment strength detection in short informal text
Journal of the American Society for Information Science and Technology, 2010, 61.12: 2544-2558.

\bibitem{sent-stemming}
Nasukawa, Tetsuya, YI, Jeonghee
Sentiment analysis: Capturing favorability using natural language processing
Proceedings of the 2nd international conference on Knowledge capture. ACM, 2003. p. 70-77.

\bibitem{realtimesent}
Ahmad, Khurshid; Cheng, David; Almas, Yousif.
Multi-lingual sentiment analysis of financial news streams.
Proc. of the 1st Intl. Conf. on Grid in Finance. 2006.

\bibitem{humandisagree}
Alastair J. Gill, Darren Gergle, Robert M. French, Jon Oberlander
Emotion Rating from Short Blog Texts
Proceedings of the SIGCHI Conference on Human Factors in Computing Systems, 2008

\bibitem{sentfrasi}
Wilson, Theresa; Wiebe, Janyce; Hoffmann, Paul.
Recognizing contextual polarity in phrase-level sentiment analysis. In: Proceedings of the conference on Human Language Technology and Empirical Methods in Natural Language Processing. Association for Computational Linguistics, 2005. p. 347-354.

\bibitem{tokenchinese}
Chang, Pi-Chuan; Galley, Michel; Manning, Christopher D.
Optimizing Chinese word segmentation for machine translation performance.
Proceedings of the Third Workshop on Statistical Machine Translation. Association for Computational Linguistics, 2008. p. 224-232.

\bibitem{snowball}
Porter, Martin F.
Snowball: A language for stemming algorithms (2001)

\bibitem{smoothlaplace}
Zadrozny, Bianca; Elkan, Charles.
Obtaining calibrated probability estimates from decision trees and naive Bayesian classifiers.
machine learning-international workshop then conference 2001. p. 609-616.
\end{thebibliography}

\end{document}
